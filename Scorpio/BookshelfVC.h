//
//  BookshelfVC.h
//  Scorpio
//
//  Created by Jean-François Bouverat on 16/12/2014.
//  Copyright (c) 2014 Jean-François Bouverat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSPDFKit/PSPDFKit.h>

@interface BookshelfVC : UICollectionViewController

@end
