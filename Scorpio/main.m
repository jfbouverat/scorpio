//
//  main.m
//  Scorpio
//
//  Created by Jean-François Bouverat on 23/09/2014.
//  Copyright (c) 2014 Jean-François Bouverat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
