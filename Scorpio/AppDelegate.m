//
//  AppDelegate.m
//  Scorpio
//
//  Created by Jean-François Bouverat on 23/09/2014.
//  Copyright (c) 2014 Jean-François Bouverat. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    PSPDFSetLicenseKey("IqXfGyomDljdXiedWbuUGV/JrYEYL2ZtUbSubpJwDM4zXAqhVOOAlb2nNJaV"
                       "TPW4avtUNplbC6QOoVf3aNiVhfwnTqEYQWdmzdA34QkKZHmDS6QsEoSLj6wP"
                       "TRlHc9UqysXnrXiIQrHzMl5lFSzoaYZGzn+/bqwXR17nSZiZEjEVpOhJPui7"
                       "2UuMTz9TpxGJEFapV9pJA/UmxBI1LbS7+jvLeFk7gJT3fOvXmHHlxm45W6E+"
                       "YqLVTlosijtPxfAfM4fD9SZWh/9kF4+GTr/0R3haTKsBABYpuaX5KlvuhjdV"
                       "puKrB6hjJQ7NiBLw/fmT4gFUe/rf4VJYJJiXfXTFeYhCaCI4cJW5txFDW3l6"
                       "s65TCGhjmtfmAvCcX+bGvpFK1WeFryu2gPF2Kf7Nxkn5YmyFaDvw6HwrsYzp"
                       "oYNCRhywKwjciqa22Sp7ZRvLsqgQPiIqEF4iCil86e/bKvGEnQ==");
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
