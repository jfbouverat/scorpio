//
//  AppDelegate.h
//  Scorpio
//
//  Created by Jean-François Bouverat on 23/09/2014.
//  Copyright (c) 2014 Jean-François Bouverat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSPDFKit/PSPDFKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

